# lansce3-unpacker

Written by Shea Mosby (LANL) and Warren Rogers (IWU)


This unpacker is designed to produce ROOT files from raw data files produced by digiTES version %%% without waveform recording.  

To compile, issue the command ./build_libraray from the default directory for the distribution

The config_coefs.txt contains calibration coefficients for time and position calibration of the detector array.  The newest_config_coefs.txt file contains all of the calibration data for the Fall 2023 LANSCE-3 run at Los Alamos.  

To run the unpacker, take a look at the shell script "unpack" to see which config file it refers to when unpacking.  To unpack a "raw.dat" file, issue the following:

./unpack raw.dat outfile.root