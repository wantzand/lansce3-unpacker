#!/usr/bin/python
import os
import subprocess
import sys
import math

runnumber = sys.argv[1]
nke = sys.argv[2] 
fwhm = sys.argv[3]
c = float(29.98)
distance = float(8763 + 105)
mcsquare = float(939.56)
toflow = (distance)/(c*math.sqrt(1-((mcsquare)/(((1-float(fwhm))*(float(nke)))+mcsquare))**2))
tofhigh = (distance)/(c*math.sqrt(1-((mcsquare)/(((1+float(fwhm))*(float(nke)))+mcsquare))**2))
outfile = open(runnumber[0:4] + "-filter.C","w")
chain = "{\ngROOT->ProcessLine(" + "\"" + ".x " + runnumber + "\"" + ");\n"
outfile.write(chain)
newfile = "TFile *f = new TFile(\"temp.root\",\"recreate\");\n"
outfile.write(newfile)
copytree1 = "TTree *t_filter = tree->CopyTree(\"(tmean[0]-t0t>"
copytree2 = " && tmean[0]-t0t<"
copytree3 = ") || (tmean[0]-t0t>50 && tmean[0]-t0t<300)\",\"fast\");\n"
#copytree3 = ") \",\"fast\");\n"
copytree = copytree1 + str(tofhigh) + copytree2 + str(toflow) + copytree3
#copytree = copytree1 + "2000" + copytree2 + "1e6" + copytree3
outfile.write(copytree)
writefile = "t_filter->Write();\n}"
outfile.write(writefile)
rootcall = "root.exe -q " + runnumber[0:4]+"-filter.C"
outfile.close()
os.system(rootcall)
newfilename = runnumber[0:4] + "-" + nke + ".root"
#newfilename = runnumber[0:4] + "-" + "bkg" + ".root"
rename_call = "mv temp.root " + newfilename
os.system(rename_call)
#print("File " + sys.argv[1] " is filtered :)")
