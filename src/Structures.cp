#define MaxHitsPerT0 60000
typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;
typedef int32_t INT;

#define NMONABARS 24 

// Internal ELSA data structure
typedef struct {
    double time;
    double finetimestamp;
    uint16_t integral[2];
    uint16_t detector_id;
} ELSA_BANK;

typedef struct {
  double tmean;
  double qmean;
  double tm_cal;
  double tleft;
  double tright;
  double qleft;
  double pleft;
  double qright;
  double pright;
  int barnum;
  int channel;
  bool iscomplete;
  double veto_t;
  double veto_q;
  double t0offset;
} MONABAR_HIT;

typedef struct {
  double qleft[NMONABARS];
  double qright[NMONABARS];
  double pleft[NMONABARS];
  double pright[NMONABARS];
  double tmean[NMONABARS];
  double tm_cal[NMONABARS];
  double qmean[NMONABARS];
  double qmeana[NMONABARS];
  double psd[NMONABARS];
  double qmeancal[NMONABARS];
  double tdiff[NMONABARS];
  double tleft[NMONABARS];
  double tright[NMONABARS];
  double x[NMONABARS];
  double y[NMONABARS];
  double z[NMONABARS];
  int ch[NMONABARS];
  int bar[NMONABARS];
  int mult;
  std::vector<ELSA_BANK> coincevts;
  double veto_t;
  double veto_q;
  double muonbkg;
//  double bar_xslope[NMONABARS];
//  double bar_ypos[NMONABARS];
//  double bar_zpos[NMONABARS];
} MONA_EVENT;

typedef struct
{ 
    int32_t Ns;                                         // Num of samples
    uint16_t *AnalogTrace[2];           // Analog traces (samples); the 2nd trace is enabled if DualTrace=1
    uint8_t  *DigitalTraces;            // Digital traces (1 bit = 1 trace)
        int8_t DualTrace;                               // Dual Analog Traces
        int8_t TraceSet[6];   // Traces Setting
} Waveform_t;

typedef struct {
        uint64_t TimeStamp;                     // 64 bit coarse time stamp (in ns)
        uint16_t FineTimeStamp;         // Fine time stamp (interpolation) in ps
        uint16_t Energy;                        // Energy (charge for DPP_PSD/CI or pulse height for DPP_PHA)
        float psd;                                      // Pulse Shape Discrimination (PSD = (Qlong-Qshort)/Qlong). Only for DPP_PSD. Range 0 to 1023.
        Waveform_t *Waveforms;          // Pointer to waveform data (NULL if the waveform mode is not enabled)
        uint16_t Flags;                         // Event Flags (e.g. pile-up, saturation, etc...)
} DEVT_BANK;
