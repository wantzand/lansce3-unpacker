#!/usr/bin/env python2
import elsa
import sys

# NOTES
# dtype is for input datastream type - 7 for initial unpacking of raw binary file without waveforms (11/19 experiment)
# and 5 for raw binary file with waveforms (1/16 experiment), and 4 is for second stage event building for mona from secondary 
# s0bin binary file produced from raw binary file.  All options are listed in the header of elsa.py
# 

if __name__=='__main__':
    # single threaded basic testing
    testclass = elsa.Elsa()
    if len(sys.argv) < 6:
#        sys.exit('Usage: %s <run number> <t0 build> <board offset type> <stage number>'%(sys.argv[0]))
        sys.exit('Usage: %s <infile.bin> <outfile.root> <t0 build> <board offset type> <stage number>'%(sys.argv[0]))

    # detector class setup
    for i in range(50):
        testclass.detclass.append(1)
        testclass.tslope.append(1.0) # For CAEN 1730 500 MHz digitizer -- 1.0 makes the time in ns for the run work correctly (plot tmean[0] for example).  2.0 gives twice the duration as the actual run 
        testclass.toffset.append(0.0)
        testclass.tdoffset.append(0.0)
        testclass.qmeancal.append(0.0)
        testclass.eslope.append(1.0)
        testclass.eoffset.append(0.0)
        testclass.polarity.append(-1.0)
        testclass.bar_xslope.append(1.0)
        testclass.bar_ypos.append(1.0)
        testclass.bar_zpos.append(1.0)
    testclass.detclass[16] = 0 # for when the t0 signal is in channel 16.  Modify this as needed

    testclass.STOPatEVENT = 2000000000  #2e9
#    testclass.STOPatEVENT = 10000000
#    testclass.STOPatEVENT = 4.0e7 

# **********
    # Read offset and calibration coefficient files
    infile_config_coefs = open("./config_coefs.txt","r")
    # First the time offsets, global first and then each bar
    comment = infile_config_coefs.readline()
    toffsets = infile_config_coefs.readline()
    entry = toffsets.split(" ")
    testclass.goffset = float(entry[0]) # Global time offset (ns) to locate gamma flash in time

    for i in range(16): # Time offsets for first 8 bars in card 0
        testclass.toffset[i] = float(entry[i+1]) 

    for i in range(14):
        testclass.toffset[i+32] = float(entry[i+17]) # Time offsets for the second set of 7 bars in card 2

    testclass.toffset[17] = float(entry[31]) # Diamond 1 -2 to 2 V range
    testclass.toffset[18] = float(entry[32]) # Diamond 1 -0.5 V to 0.5 V range
    testclass.toffset[19] = float(entry[33]) # Diamond 2 -2 to 2 V range
    
    # Next the tdiff offsets for each detector
    comment = infile_config_coefs.readline()
    tdoffsets = infile_config_coefs.readline()
    entry = tdoffsets.split(" ")
    for i in range(17):
        testclass.tdoffset[i] = float(entry[i]) 

    # Next the qmean calibration coefs to convert to MeV
    comment = infile_config_coefs.readline()
    qmeancoefs = infile_config_coefs.readline()
    entry = qmeancoefs.split(" ")

    for i in range(16):
        testclass.qmeancal[i] = float(entry[i]) # First 8 bars in board 0

    for i in range(14):
        testclass.qmeancal[i+32] = float(entry[i+16]) # Next 7 bars in board 2

    testclass.qmeancal[17] = float(entry[30]) # Diamond channels
    testclass.qmeancal[18] = float(entry[31])
    testclass.qmeancal[19] = float(entry[32])

    comment = infile_config_coefs.readline()
    # Next the positions for each MoNA bar
    for i in range(15):
        barpositions = infile_config_coefs.readline()
        entry = barpositions.split(" ")
        testclass.bar_xslope[i] = float(entry[0])
        testclass.bar_ypos[i] = float(entry[1])
        testclass.bar_zpos[i] = float(entry[2])

    # Positions for the diamond detectors
    diapositions = infile_config_coefs.readline()
    entry = diapositions.split(" ")
    testclass.bar_xslope[15] = float(entry[0])
    testclass.bar_ypos[15] = float(entry[1])
    testclass.bar_zpos[15] = float(entry[2])

    diapositions = infile_config_coefs.readline()
    entry = diapositions.split(" ")
    testclass.bar_xslope[16] = float(entry[0])
    testclass.bar_ypos[16] = float(entry[1])
    testclass.bar_zpos[16] = float(entry[2])
    infile_config_coefs.close()
# **********

#    testclass.ifname = '/home/analysis/lanl-analysis/elsa/raw/run%05i.bin'%(int(sys.argv[1]))
    testclass.ifname = sys.argv[1] 
#    testclass.ofname = 'stage0/run%05i_s0.bin'%(int(sys.argv[1]))
    testclass.ofname = 'temp.bin' 

    testclass.dtype = 7 # datastream type: 5 for mona/lanl raw files with waveforms, 7 for without (see elsa.py for all options)
    testclass.ftoption = 0
    testclass.extra_option = 0 # 1 to write pulse heights, 0 to not -- I think this has to do with waveform recording
    testclass.t0ds = 0 # Downscale variable to determine how many t0 signals get written to the datafile
#    print ("t0 downscale factor %i"%testclass.t0ds)
    if int(sys.argv[5])==0:
        elsa.run_elsa(testclass)

    # set up mona channel map
    for i in range(50):
        testclass.detid2side.append(0)
        testclass.detid2bar.append(0)
    leftrange = [0, 2, 4, 6, 8, 10, 12, 14, 32, 34, 36, 38, 40, 42, 44, 17, 19] # 15 MoNA bars and two diamond detectors, left (2V scale for diamonds)
    rightrange = [1, 3, 5, 7, 9, 11, 13, 15, 33, 35, 37, 39, 41, 43, 45, 18] # channels 17 through 20 (bars 15-16 are two diamonds with two channels each, channel 16 is t0

    # In the following, "count" is the 0-based index number and "i" is the entry value at that array index
    for count,i in enumerate(leftrange):
        testclass.detid2side[i] = 0
        testclass.detid2bar[i] = count
    for count,i in enumerate(rightrange):
        testclass.detid2side[i] = 1
        testclass.detid2bar[i] = count

#    testclass.dump0r = 1 # decide where to put root file, in stage1 (1) or stage0 (0) directory.  
#    testclass.ifname = 'stage0/run%05i_s0.bin'%(int(sys.argv[1])) 
    testclass.ifname = 'temp.bin' 
#    if testclass.dump0r:
#        testclass.ofname = 'stage1/run%05i_s0.root'%(int(sys.argv[1])) 
#        testclass.ofname = sys.argv[2] 
#        testclass.ofname = 'final.root'
    testclass.ofname = sys.argv[2]
#    else:
#        testclass.ofname = 'stage0/run%05i_s0.root'%(int(sys.argv[1])) 
#        testclass.ofname = sys.argv[2] 
#        testclass.ofname = 'temp_s0.root' 
    testclass.t0ebuild = int(sys.argv[3]) # Set to 1 to trigger on T0, 0 for no T0 trigger
    print ("t0ebuild (1=beam, 0=cosmics) %i"%int(sys.argv[3]))

    # Set coefs to adjust time and time diff to account for board timing offsets
    if int(sys.argv[4])==0: # run06040 type files
    	testclass.b0offset = 0.0
    	testclass.b1offset = 0.0 
    if int(sys.argv[4])==1: # run06180 type files
        testclass.b0offset = -7.7
        testclass.b1offset = 16.57 
    if int(sys.argv[4])==2: # run06003 type files
        testclass.b0offset = -8.0
        testclass.b1offset = 16.0 
    if int(sys.argv[4])==3: # run06003 type files
        testclass.b0offset = -8.0
        testclass.b1offset = 14.8 
    
    print ("testclass.b1offset final = %f"%testclass.b1offset)
    testclass.ebuild = 3 # 3 = mona event build, 0 = just raw data
    testclass.dtype = 4 # 4 = input datastream is stage0 binary file - see elsa.py for options
    testclass.calibrate_time = 1
    testclass.calibrate_energy = 0
    if int(sys.argv[5])==1:
        elsa.run_elsa(testclass)
