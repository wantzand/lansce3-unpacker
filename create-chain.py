#!/usr/bin/python
import os
import subprocess

os.system("ls *.root > filenames")
infile = open("filenames","r")

filenames = infile.read()
infile.close()

fnames = filenames.split("\n")

outfile = open("runchain.C","w")
outfile.write("{\nTChain* runchain = new TChain(\"t\");\n\n")
for fname in fnames:
	if fname == '': 
		break
	outfile.write("runchain->Add(\"");
	outfile.write(fname);
	outfile.write("\");\n")
outfile.write("}")
outfile.close()
os.system("rm filenames")
