import ROOT as r
r.gSystem.Load("elsa_C.so")
from multiprocessing import Pool
import array
import os.path


class Elsa:
    def __init__(self):
        #self.eroot = r.elsa_root()

        # file read options
        self.rnum = 0 # run number to process
        self.ifname = '' # path to input file
        self.wfrm = 1 # remove waveforms from data stream
        self.dtype = 0 # data stream type
                       # 0 = UAC QILS banks
                       # 1 = DANCE UAC banks
                       # 2 = ANNA binary output
                       # 3 = OLAF binary output
                       # 4 = ELSA stage 0 binary output
                       # 5 = DEVT structure binary output - single trace
                       # 6 = DEVT structure binary output - dual trace
                       # 7 = DEVT structure binary output - NO trace
        self.interp_slope = 1.0 # deal with UAC bug where interpolated time goes the wrong way

        self.MidasEventPrint = 0
        self.MidasEventPrintThresh = 0
        self.STOPatEVENT = 0 

        # file write options
        self.dump0b = 1 # dump stage 0 binary output
        self.dump0r = 0 # dump stage 0 root output
        self.ofname = '' # path to output file

        # T0 handling
        self.t0ds = 0 # t0 downscale factor
        # self.t0ds = 15 # t0 downscale factor
        self.t0to = 1e6 # t0 timeout in ns - resets the downscaler counter

        # detector description
        self.ndets = 48 # number of detectors in the system
        self.nconvention = 0 # detector numbering convention
                             # 0 = UAC (starts at 1)
                             # 1 = ANNA / OLAF (starts at 0)
        self.detclass = []
        # timing options
        self.tcalibrate = 0 # use timing offsets
        self.ftoption = 1 # fine time option
                          # 0 = no fine time
                          # 1 = firmware time
                          # 2 = digital CFD - requires waveform processing
                          # 3 = digital double derivative - requires waveform processing
                          # 4 = DANCE timing algorithm
                          # 5 = asymmetrically smoothed CFD (experimental)
        self.polarity = [] # timing filter params for waveform time extractions

        # calibration options
        self.calibrate_energy = 0 # use energy slopes / offsets
        self.calibrate_time = 0 # use energy slopes / offsets
        self.tslope = []
        self.toffset = []
        self.tdoffset = []
        self.qmeancal = []
        self.eslope = []
        self.eoffset = []
        self.bar_xslope = []
        self.bar_ypos = []
        self.bar_zpos = []

        # event build controls
        self.ebuild = 0 # whether to build events at all
                        # 0 = don't build events
                        # 1 = build chi-nu style events
                        # 2 = build dance style events
                        # 3 = build mona style events
        self.t0ebuild = 0 # whether to consider t0s as part of event build
                          # 0 = no
                          # 1 = yes
        self.b0offset = 0 # how to compile file based on VME board relative time offsets
        self.b1offset = 0 # how to compile file based on VME board relative time offsets
        self.goffset = 0  # global time offset for all detectors

        # auxiliary mona stuff
        self.detid2side = []
        self.detid2bar = []


    def convert_args(self):
        # the class we're really hooking into
        self.eroot = r.elsa_root()

        self.eroot.ipath = r.TString(self.ifname)
        self.eroot.opath = r.TString(self.ofname)
        self.eroot.ftoption = self.ftoption
        temp = array.array('i',self.detclass)
        self.eroot.detclass = temp

        self.eroot.tslope = array.array('d',self.tslope)
        self.eroot.toffset = array.array('d',self.toffset)
        self.eroot.tdoffset = array.array('d',self.tdoffset)
        self.eroot.qmeancal = array.array('d',self.qmeancal)
        self.eroot.eslope = array.array('d',self.eslope)
        self.eroot.eoffset = array.array('d',self.eoffset)
        self.eroot.bar_xslope = array.array('d',self.bar_xslope)
        self.eroot.bar_ypos = array.array('d',self.bar_ypos)
        self.eroot.bar_zpos = array.array('d',self.bar_zpos)
        # t0 downscaling
        self.eroot.t0ds = self.t0ds
        self.eroot.t0to = self.t0to
        self.eroot.goffset = self.goffset
        
        # event printing
        self.eroot.MidasEventPrint = self.MidasEventPrint
        self.eroot.MidasEventPrintThresh = self.MidasEventPrintThresh
#        self.eroot.STOPatEVENT = 200000 # wfr 20220715
        self.eroot.STOPatEVENT = self.STOPatEVENT

        # file writing
        self.eroot.dump0b = self.dump0b # dump stage 0 binary output
        self.eroot.dump0r = self.dump0r # dump stage 0 root output
        self.eroot.extra_option = self.extra_option

        # waveform processing
        if len(self.polarity)>0:
            self.eroot.polarity = array.array('f',self.polarity)

        # auxiliary mona stuffs
        self.eroot.b0offset = self.b0offset # added by WFR 12/2019
        self.eroot.b1offset = self.b1offset # added by WFR 12/2019
        if len(self.detid2side)>0:
            self.eroot.detid2side = array.array('i',self.detid2side)
        if len(self.detid2bar)>0:
            self.eroot.detid2bar = array.array('i',self.detid2bar)

        return

    def read_tcal_fromfile(self,fname):
        if os.path.isfile(fname):
            for line in open(fname):
                templine = line.split()
                self.toffset[int(templine[0])] = float(templine[1])
        else:
            print ("cannot find file",fname,"so use tcals of 0")

    def read_tshift_fromfile(self,fname):
        if os.path.isfile(fname):
            for line in open(fname):
                templine = line.split()
                self.toffset[int(templine[0])] += float(templine[1])
        else:
            print ("can't find file",fname,"so use existing tcals")


def run_elsa(args):
    args.convert_args()
    if args.dtype == 0:
        args.eroot.execute_uac_qils(args.interp_slope)
    elif args.dtype == 4: # mona data
        args.eroot.execute_s0bin(args.calibrate_time,args.calibrate_energy)
    elif args.dtype == 5 or args.dtype==6 or args.dtype==7:
        args.eroot.execute_devt(args.calibrate_time,args.calibrate_energy,args.dtype)

    if args.ebuild == 1:
        args.eroot.build_events_chinu(args.t0ebuild)
    elif args.ebuild == 3:
        args.eroot.build_events_mona(args.t0ebuild)

def run_multithread_elsa(arglist,ncpu):
    p = Pool(int(ncpu))
    p.map(run_elsa,arglist)
